# taurus-training

Materials for the taurus workshop that will be held during the 38th Tango Community Meeting at SOLEIL.

# Introduction
This materials are for people who is interested to start using **Tango** as a control system framework. Training starts on Taurus provided tools for non developers. Last part will be more coding-focused, explaining how to interact with _Taurus_ programmatically.


# What you will need?
This trainings are planned to use Tango control system. Before starting the exercises, we will need:
* A complete installation of a TangoDB, access to TangoDB over the network, or TangoDB running via Docker.
    * You can find information on how to install Tango [here](https://tango-controls.readthedocs.io/en/latest/installation/index.html).
    * Here you have information on how to use Tango with docker [here](https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html).
* An installation of the [TangoTest](https://gitlab.com/tango-controls/TangoTest) device server or one that is available from the TANGO_HOST we connect to.
* An installation of [conda](https://docs.anaconda.com/free/miniconda/).
* Creation of a conda environment from the YAML file available in the repository, inside utils folder, using `conda env create -f taurus_training_environment.yml`.
* We can extend the conda installation with [Jive](https://anaconda.org/conda-forge/jive) and [Astor](https://anaconda.org/conda-forge/tango-astor).
* For SVGSYNOPTIC training you will need [Inkscape](https://inkscape.org/release/inkscape-1.3.2/).