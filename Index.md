# Taurus Training

## Introduction
This Taurus Training serves as a hands-on guide to start using Taurus in a basic level or deepen into its features and options. Here you can find the table of contents with links to each guide.

## Table of contents
1. Zero Code  
1.1. [Taurus Widgets - Form](TaurusWidgets-Form.md): Use of TaurusFrom and eval()  
1.2. [Taurus Plotting Tools](TaurusPlottingTools.md): Use of TaurusTrend and TaurusPlot  
1.3. [Taurus GUI](TaurusGUI.md): Create a Taurus GUI without code  
2. Programatic  
2.1. [Taurus GUI (Programatic)](TaurusProgramaticGUI.md): Create a Taurus GUI with code  
2.2. [Taurify Widgets](TaurifyWidgets.md): Convert Qt widgets to Taurus widgets  
2.3. [Taurus SVG Synoptic](TaurusSVGSynoptic.md): Use of TaurusSynopticWidget  
3. No GUI  
3.1. [Taurus Core](TaurusCore.md): Use Taurus without a TaurusApplication