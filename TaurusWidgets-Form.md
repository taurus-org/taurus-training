# Taurus Basic Commands


## Introduction
We can consider Taurus as a framework that incorporates a series of tools to help us interact with a control system. As seen in the introduction, we can use Taurus from a user perspective who may not have any programming experience since, for their daily work and the type of interaction they have with the surrounding ecosystem, it is not necessary. As a starting point, we will begin to discuss, in some depth, those tools that Taurus offers us to interact with different control systems, including Tango, but without the need of any programming knowledge.

## Tools
The tools provided by Taurus can be grouped into 3 main categories:
* Widget-type Tools.
* Trend and Plot-type Tools.
* GUI-type Tools.

Next, we will discuss each of them and the relationship between each one.

## Widget Tools
Let's start with the seemingly easiest part, which is nothing more than the most basic widgets for interacting with the control system.

### [TaurusForm](https://www.taurus-scada.org/users/ui/forms.html)
It is the standard widget with which Taurus presents elements to provide us with information within the control system.

<p align="center">
    <img src="img/taurus_form_1.png" alt="TaurusForm" />
</p>

As you can see in the previous image, the widget is empty. What we are going to do now is provide it with the necessary information so that it fulfills its function and shows us the control system data. It may be useful before starting this series of exercises to have some basic knowledge of some concepts of the Taurus core, which you can see [here](https://taurus-scada.org/devel/core_tutorial.html).

#### Exercise 0
Please, go to [README - What we will need?](./README.md#what-you-will-need) and be sure that all the necessary packages are installed.

### Exercise 1: Open TaurusForm with Attribute/Attributes. Taurus Colors

Let's open a TaurusForm with a multiple attributes. To do this:
1. Open a terminal
2. Execute the following command:
    ```
    taurus form sys/tg_test/1/state sys/tg_test/1/status sys/tg_test/1/boolean_scalar sys/tg_test/1/double_scalar sys/tg_test/1/double_scalar_w sys/tg_test/1/ampli
    ```
The result should look similar to the image shown below:

<p align="center">
    <img src="img/taurus_form_2.png" alt="TaurusForm" />
</p>

Simple, right? This image provides some information, so let's start with the central part of the widget. Essentially, what we see is as follows:
<p align="center">
    <img src="img/taurus_form_3.png" alt="TaurusForm" />
</p>
<center>Basic parts of Taurus Form</center>

1. This corresponds to what we will now call _Label_. It is the name that our control system assigns to this attribute. In our example, the attribute name matches this _Label_, but it may not always be the case.
2. This is the _ReadWidget_. This widget shows the read value of the attribute, the same value that we would get if we tried to read the same attribute in a different way, such as from a terminal, a script, or another control system application.
3. This is the _WriteWidget_. This widget **only** appears if it is possible to write to the attribute. Its value and style may vary, depending on whether the internal value matches the actual value. The widget style changes to indicate that the _WriteWidget_ value has not yet been applied and it differs from the Tango write value, as shown in the following image:

<p align="center">
    <img src="img/taurus_form_4.png" alt="TaurusForm" />
</p>

Once the value is applied, in this case by simply pressing the _Enter_ key, everything returns to the initial state:

<p align="center">
    <img src="img/taurus_form_5.png" alt="TaurusForm" />
</p>

4. This is the _UnitsWidget_. Taurus incorporates the [pint](https://pint.readthedocs.io/en/stable/) library. As long as the units are within those recognized by the International System, pint can perform unit and magnitude conversions. For more details, you can refer to [this guide](https://pint.readthedocs.io/en/stable/user/defining-quantities.html).

We have now interacted with our control system. First, we opened TaurusForm with a attribute we wanted to monitor, changed its value, and observed the change. If we close the widget and reopen it, we will see that the value we set is keep, indicating that we have used Taurus to interact with our control system.

Before we proceed further, let's explore another way to add attributes to _TaurusForm_. Imagine we don't know the exact name of the attribute or attributes we want to display. The previous example would leave us stuck, requiring a level of knowledge and memory we don't always have. Let's see how Taurus addresses this situation.

1. Open a terminal
2. Execute the command: `taurus form`
3. A dialog called [_ModelChooser_](https://taurus-scada.org/users/ui/modelchooser.html) will appear, as shown below:

<p align="center">
    <img src="img/taurus_form_6.png" alt="TaurusForm" />
</p>

4. Navigate through the control system to find the attribute you want to add:

<p align="center">
    <img src="img/taurus_form_7.png" alt="TaurusForm" />
</p>

5. Add the attribute by clicking on the _+_ symbol in the central part.

<p align="center">
    <img src="img/taurus_form_7_bis.png" alt="TaurusForm" />
</p>

6. Click _Apply_ and close the dialog.
<p align="center">
    <img src="img/taurus_form_8_bis.png" alt="TaurusForm" />
</p>

Now _TaurusForm_ should contain the previously selected attribute.

Let's imagine we forgot to introduce a attribute to our widget. If we had to restart our application constantly, it would be impractical. Taurus anticipates this, so let's see how to use the application's context menu to reopen the previous dialog and add more attributes to our widget. For now, we will be focusing only on this, because the context menu offers other options that we will explore later. Let's proceed:

1. Right-click within the central part of the widget that is empty, meaning it is not occupied by any widget. A context menu will appear.
2. Click on the _Modify Contents_ option.

<p align="center">
    <img src="img/taurus_form_9.png" alt="TaurusForm" />
</p>

3. The same dialog we saw earlier will appear, so we just need to follow the previous steps and add all the necessary attributes.

These are the ways we can add an attribute to the _TaurusForm_ visualization. Simple, right? We can also add more than one attribute at a time, both from the console and from [_ModelChooser_](https://www.taurus-scada.org/users/ui/modelchooser.html).
* From the console:
    1. Execute the command `taurus form` followed by the attributes you want to display space separated. Example: `taurus form sys/tg_test/1/ampli  sys/tg_test/1/double_scalar`
* From _ModelChooser_:
    1. Use the _Shift_ or _Ctrl_ key to select multiple attributes from the system.
    2. Add them by clicking the _+_ button and then _Apply_. Make sure to maintain the selection to add the attributes.

As you can see, it's not very complicated.

#### Taurus colors & _ConfigEvent_

You may have noticed that the style and color of certain widgets are a bit special. This is not random; Taurus has implemented a color code that provides additional information about what we are seeing. [Here](https://www.taurus-scada.org/users/ui/ui_colors.html) you can see the complete Taurus color guide. Let's look at an example:

0. Ensure the attribute `sys/tg_test/1/short_scalar` has the following configuration:
    * Min.Alarm = -100
    * Max.Alarm = 100
    * Min.Warning = -50
    * Max.Warning = 50

1. Execute the command `taurus form sys/tg_test/1/short_scalar`.
2. Change the value to exceed the attribute value, for example, 118.
3. Observe that the widget color changes based on the Tango Attribute Quality value.

We can also see what happens with the device state colors:
1. Execute `taurus form sys/tg_test/1/state sys/tg_test/1/status`
2. Change the test device state by executing the _SwitchStates_ command and observe the transition from _RUNNING_ to _FAULT_ state, and the widget colors change:

<p align="center">
    <img src="img/taurus_form_24.png" alt="TaurusForm" />
</p>

<p align="center">
    <img src="img/taurus_form_25.png" alt="TaurusForm" />
</p>

Note that Taurus subscribes to configuration events, so any changes in the Tango attribute configuration will automatically be reflected in our widgets.

### Exercise 2: Pending Operations, Force Apply, Apply All, Reset Form, Show/Hide Buttons

Let's finish introducing the elements presented in the widget. First of all we will take a look on 2 basic Taurus concepts:

* Pending Operations
* Force apply

#### Pending Operations
Writing to an attribute is not always safe, especially when interacting with hardware, which can cause problems. By default, Taurus does not apply values until the _Enter_ key is pressed to confirm the changes. Instead, the corresponding item label becomes highlighted with a blue frame, indicating that the write value has been changed (we say the item has pending operations) and then these changes can be applied. Some write widgets provide extra feedback apart from the label. More information [here](https://taurus-scada.org/users/ui/forms.html#writing-to-attributes).

#### Force Apply
Sometimes it may be necessary to force the writing of a specific attribute. To do that, we can use this key combination: _CTRL+Enter_. More information [here](https://taurus-scada.org/users/ui/forms.html#forced-apply).

Now we will focus on the bottom part, where we can find two buttons: _Reset_ and _Apply_.

#### Apply Button
The Apply button is used to apply pending operations within the widget. This means that if there are changes waiting to be applied, pressing this button will apply all of them simultaneously. Let's see an example:

1. Open a console.
2. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
3. Change the value of `ampli` from 0.00 to 5.00 *without pressing Enter*.
4. Change the value of `double_scalar_w` from 0.00 to 8.00 *without pressing Enter*.
5. Press the Apply button.

As we can see, the values of both attributes have been written. This is a way to apply pending operations within the widget.

#### Reset Button
The Reset button is used to reset the TaurusForm and avoid applying values by mistake. Pressing the Reset button returns the WriteWidget to the value of the Tango Attribute write value. Let's see an example:

1. Open a console.
2. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
3. Change the value of `ampli` from 0.00 to 5.00 *without pressing Enter*.
4. Change the value of `double_scalar_w` from 0.00 to 8.00 *without pressing Enter*.
5. Press the Reset button.

As we can see, the values are not applied, and the value of our WriteWidget returns to its original value.

#### Show/Hide Buttons
Let's look at another option available in the contextual menu of our TaurusForm.

1. Right-click on an area where there are no other widgets.
2. Uncheck the option `Show Buttons`.

<p align="center">
    <img src="img/taurus_form_10.png" alt="TaurusForm" />
</p>

3. The two buttons should disappear:

<p align="center">
    <img src="img/taurus_form_11.png" alt="TaurusForm" />
</p>

This feature can be useful to avoid applying values by mistake or simply to have a more compact view of the widget.

### Exercise 3: Compact Mode
As you might know, not all attributes are writable. Automatically, TaurusForm does not show the WriteWidget for these attributes since it doesn't make sense. Additionally, there are times when we might not want to display the WriteWidget, either because it's unnecessary or could potentially cause issues. For this purpose, we have what we call CompactMode. Let's see how we can apply it:

1. Open a console.
2. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
3. Right-click on an area where there are no other widgets.
4. Check the option CompactMode. Now our widget should look like this:

<p align="center">
    <img src="img/taurus_form_12.png" alt="TaurusForm" />
</p>

In this way, we can hide the WriteWidgets, avoiding unwanted interactions or simply displaying only the necessary information.

### Exercise 4: Save Current Configuration
One very useful feature provided by Taurus is the ability to save previous configurations. Taurus allows us to have a file with a previously saved TaurusForm configuration and load it at any time. Let's see how it's done:

1. Open a console.
2. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
3. Right-click on an area where there are no other widgets.
4. Select the option Save current settings. This can also be done using the keyboard shortcut Ctrl+S.
5. A dialog will open where you can choose where to save your configuration.

That's it, we have just saved the TaurusForm configuration to a file. The configuration is saved in a Pickle file. For more information about this format, you can go [here](https://docs.python.org/3/library/pickle.html).

### Exercise 5: Retrieve Configuration
Now that we know how to save the configuration, let's see how we can load it:

1. Open a console.
2. Execute `taurus form`.
3. Close the ModelChooser dialog.
4. Right-click on an area where there are no other widgets.
5. Select the option Retrieve saved settings. This can also be done using the keyboard shortcut Ctrl+O.
6. Select the previously saved file.

At this point, we should have the configuration loaded again.

### Exercise 6: Change Label, Change Units
Often, users need more information about the attribute they are viewing. Sometimes, the predefined label is insufficient or can be confusing. Taurus offers the ability to change this label to provide the user with more information about the attribute they are viewing. To do this:

1. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
2. Right-click on the _Label_ of the first attribute.
3. Select _Change Label_. A dialog like the following will appear:

<p align="center">
    <img src="img/taurus_form_17.png" alt="TaurusForm" />
</p>

4. Select the name format you want to display. Note that expressions can be used to adjust the label name to the user's preference. In the following image, we used the format _{attr.fullname}_ for the upper attribute and created an expression for the lower one: _{dev.name}/{attr.name}_. This is the result:

<p align="center">
    <img src="img/taurus_form_18.png" alt="TaurusForm" />
</p>

It is normal for users to want exact information about what is happening in their control system. Often, we like to display the values of our device attributes with the units they represent. Taurus incorporates the module [pint](https://pint.readthedocs.io/en/stable/), which provides the framework the ability to interact with magnitudes and units. If necessary, these units can be edited and changed from TaurusForm. Let's see how:

1. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
2. Right-click on the _Label_ of the first attribute.
3. Select the option _Configuration_ and then _All..._. A dialog like the following will appear:

<p align="center">
    <img src="img/taurus_form_13.png" alt="TaurusForm" />
</p>

4. Modify the unit value. Remember, it must be a unit from the International System.

<p align="center">
    <img src="img/taurus_form_14_bis.png" alt="TaurusForm" />
</p>

In the same way, you can change the _Label_, alarms, numerical output format, etc. This process is equivalent to changing the attribute's configuration from Jive, altering the information stored in the TangoDB.

#### Troubleshoot Change Units
This operation can cause problems.
##### DimensionalityError
In case of a _pint.errors.DimensionalityError: Cannot convert from 'dimensionless' (dimensionless) to ..._ error when writing the attribute:
- Reopen _TaurusForm_. The unit you set will be reloaded, and Taurus will be able to perform the conversion.

### Exercise 6: Change _WriteWidget_
The catalog of Taurus widgets is extensive enough to cover all the needs when working with our control system. For example, you can find various ways to represent attributes [here](https://www.taurus-scada.org/users/screenshots.html). Let's see how to change the _WriteWidget_:

1. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
2. Right-click on the _Label_ of the first attribute.
3. Select _Change Write Widget_. A dialog with a dropdown menu will open:

<p align="center">
    <img src="img/taurus_form_15.png" alt="TaurusForm" />
</p>

4. Select the widget that you think is most appropriate. For example:

<p align="center">
    <img src="img/taurus_form_16.png" alt="TaurusForm" />
</p>

### Exercise 7: Drag and Drop
A very useful feature provided by Taurus is the ability to perform _Drag & Drop_ between its applications. Let's perform a quick test:

1. Execute `taurus form sys/tg_test/1/ampli sys/tg_test/1/double_scalar_w`.
2. Execute `taurus form` and close the model chooser.
3. Drag the _ampli_ label to the _TaurusForm_ window that we just opened. We should see the model being added to the new form.

### Exercise 8: If the Model is a Matrix or an Image...
As we know, control systems do not only display scalar data. Often, we can encounter data in the form of arrays or matrices, such as images. If what we are trying to represent is an image or an array, _TaurusForm_ will display it as follows:

<p align="center">
    <img src="img/taurus_form_19.png" alt="TaurusForm" />
</p>

What happens is that _TaurusForm_ will show a button that serves as a launcher for another application.

#### In the Case of an Image
It will display the image in a specific widget for images called [_TaurusImageDialog_](https://taurus-scada.org/users/ui/taurusimage.html). If we click on the button to the left of _double_image_ro_:

<p align="center">
    <img src="img/taurus_form_20.png" alt="TaurusForm" />
</p>

#### In the Case of an Array
It will display the array in a specific widget called _TaurusPlot_, which we will discuss later. If we click on the button to the left of _double_spectrum_ro_:

<p align="center">
    <img src="img/taurus_form_21.png" alt="TaurusForm" />
</p>

### Exercise 9: If the Model is a Device... (Not an Attribute)
If we pass a Tango device to _TaurusForm_ instead of an attribute, we will see a shortcut to a _TaurusDevicePanel_ like the following:

<p align="center">
    <img src="img/taurus_form_22.png" alt="TaurusForm" />
</p>

It can also be launched directly using the command `taurus device DEVICE_NAME`.

As we can see, a new panel opens with all the contents of the device, not only attributes but also commands.

### Using `eval` in Taurus

The `eval` tool in Taurus allows users to evaluate expressions and display the results within a widget. This powerful feature enables dynamic interaction with data, making it possible to manipulate and visualize control system data in flexible and innovative ways. Below, we provide a comprehensive guide on using `eval` in Taurus, followed by exercises to illustrate its application.

#### What is `eval` in Taurus?
The `eval` tool in Taurus is used to create and evaluate expressions involving control system data. It allows the combination of different data sources and the application of mathematical operations to these sources, providing enhanced insights and customized data views.

#### Basic Syntax
The basic syntax for an `eval` expression is:
```
eval:'expression'
```
Where `expression` is a Python expression that can include control system data references and mathematical operations.

#### Exercise 10: Basic Arithmetic Operation
Execute:`taurus form eval:'2 + 2'`

This will display the result of the arithmetic operation (4) in the Taurus form.

#### Exercise 11: Combining Attributes
Execute: `taurus form eval:'a={tango:sys/tg_test/1/ampli};b={tango:sys/tg_test/1/double_scalar_w}; a + b'`
This will evaluate the sum of two attributes and display the result.

#### Exercise 12: Adding Noise to an Image

Execute:`taurus form eval:'img={tango:sys/tg_test/1/short_image_ro}; img + 10 * rand(*img.shape)'`

This evaluates an image attribute, adds noise to it, and displays the noisy image.

#### Exercise 13: Using python library tools

Execute: `taurus form 'eval:@os.*/path.exists("/tmp/foo")'`

This evaluates if a path exists.

#### Resources
For more details and examples on using `eval` in Taurus, refer to the [Taurus Evaluation API](https://taurus-scada.org/devel/api/taurus.core.evaluation.html).

By using the `eval` feature, users can harness the full potential of Taurus to create customized and dynamic data visualizations and analyses, enhancing their control system's operability and data insights.
