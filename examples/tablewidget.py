
from taurus.external.qt import QtWidgets
from taurus.qt.qtgui.base import TaurusBaseComponent
from taurus.qt.qtgui.application import TaurusApplication


class TaurusTable(QtWidgets.QTableWidget, TaurusBaseComponent):

    """
     A Taurus-fied QWidgetTable for a TaurusAttribute type list.
    """
    setModel = TaurusBaseComponent.setModel

    def __init__(self, columns, rows, hor_head=0, *args, **kwargs):
        """
        Init of the class.

        :param columns: Initial number of columns
        :param rows: Initial number os rows
        :param hor_head: Enable the user to hide the Horizontal Headers from
        the table. If the user insert 1 on this parameter, the horizontal
        headers will not shown.

        """
        super(TaurusTable, self).__init__(*args, **kwargs)
        # super(TaurusBaseComponent, self).__init__('TaurusBaseComponent')

        self._rows = rows

        self.setRowCount(rows)
        self.setColumnCount(columns)

        if hor_head == 1:
            self.horizontalHeader().hide()

    def handleEvent(self, evt_src, evt_type, evt_value):
        """
        Reimplemented form TaurusBaseComponent.

        We read one attribute that is a list of Bools. We show the
        attribute list in the table in the first N rows.

        :param evt_src: (object or None) object that triggered the event
        :param evt_type: (taurus.core.taurusbasetypes.TaurusEventType or None)
        type of event
        :param evt_value: (object or None) event value

        `See Taurus Docs <http://www.taurus-scada.org/en/latest/devel/api/taurus/qt/qtgui/base/_TaurusBaseWidget.html>`_.

        """

        val = evt_value.rvalue
        for j, v in enumerate(val[0:self._rows]):
            self.setItem(j, 0, QtWidgets.QTableWidgetItem(str(v)))


if __name__ == "__main__":

    import sys

    app = TaurusApplication()

    w = TaurusTable(1, 10, 1)
    w.setModel("sys/tg_test/1/boolean_spectrum_ro")

    w.show()
    sys.exit(app.exec_())
