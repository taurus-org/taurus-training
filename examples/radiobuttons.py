from taurus.external.qt import Qt
from taurus.qt.qtgui.base import TaurusBaseWritableWidget
from taurus.qt.qtgui.application import TaurusApplication

class RadioOptions(Qt.QWidget, TaurusBaseWritableWidget):


    def __init__(self, parent=None, designMode=False, name=None, options={}):
        self.call__init__wo_kw(Qt.QWidget, parent)
        self.call__init__(
            TaurusBaseWritableWidget, name, designMode=designMode
        )

        self.setAutoApply(True)

        self.options = {}
        layout = Qt.QGridLayout()
        pos = 0

        for option in options.keys():
            radiobutton = Qt.QRadioButton(option)
            radiobutton.clicked.connect(self.radioButtonSelected)
            layout.addWidget(radiobutton, pos, 0)
            self.options[option] = {
                "value":options[option],
                "button": radiobutton
            }
            pos += 1

        self.setLayout(layout)


    def handleEvent(self, evt_src, evt_type, evt_value):
        val = evt_value.rvalue
        self.setValue(val)


    def setValue(self, v):
        for data in self.options.values():
            radio = data["button"]
            radio.setChecked(data["value"] == v)
       
    def getValue(self):
        for data in self.options.values():
            button = data["button"]
            if button.isChecked():
                return data["value"]

        return 0

    def radioButtonSelected(self):
        self.notifyValueChanged()

if __name__ == "__main__":

    import sys

    app = TaurusApplication(app_name="Taurified radio buttons")

    options ={
        "Option 1 -> Sets attribute to 0": 0,
        "Option 2 -> Sets attribute to 1": 1,
        "Option 3 -> Sets attribute to 2": 2,
        "Option 4 -> Sets attribute to 3": 3
    }
    w = RadioOptions(options=options)
    w.setModel("sys/tg_test/1/ampli")

    w.show()
    sys.exit(app.exec_())