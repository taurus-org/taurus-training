# Taurus Core


## Introduction
Taurus can be used whithout a graphical interface. Its core provides an abstraction of the control system and provides some tools and features that may be useful in lots of cases.

## taurus
Let's start with some of the basic capabilities of taurus. Using the basic module you can check some information about the taurus intallation like the version. It also provides a custom logger so you can easily log information in different log levels and change level you want to see. There are also some useful methods like _isValidName()_.
```
import taurus

print(taurus.__version__)

taurus.info("Info 1")
taurus.debug("Debug 1")
taurus.setLogLevel(taurus.Debug)
taurus.info("Info 2")
taurus.debug("Debug 2")

print("-"*20)
taurus.info(f"sys/tg_test/1/ampli is valid? {taurus.isValidName('sys/tg_test/1/ampli')}")
taurus.info(f"sys/tg_test/1/ampli/1 is valid? {taurus.isValidName('sys/tg_test/1/ampli/1')}")
taurus.info(f"sys/tg_test/1/nonexistent is valid? {taurus.isValidName('sys/tg_test/1/nonexistent')} "
"-> The name is valid even if the attribute or device doesn't exist.")
```


## TaurusDevice
A TaurusDevice is an object abstraction of a device. This object includes some general functions and attributes like the state (a Taurus representation of the state, not the attribute state of the device).  
Using this TaurusDevice you can easily execute commands or read attributes. Both read and execute command can be done in two ways: using specific methods (_read_attribute(attr_name)_ and _command_inout(command)_) or as attributes/methods of the device (_.attr_name_ and _.command()_).


```
import taurus
#taurus.setLogLevel(taurus.Debug)

device = taurus.Device("sys/tg_test/1")
device.info(device.state)

device.info("-"*20)

device.debug(device.read_attribute("State"))
device.info(device.read_attribute("State").value)
# Running command as function
device.SwitchStates()
device.info(device.read_attribute("State").value)
# Running command using command_inout
device.command_inout("SwitchStates")
device.info(device.read_attribute("State").value)

device.info("-"*20)

# Running commands with parameters
device.info(device.DevUShort(1))
device.info(device.command_inout("DevUShort", 2))

device.info("-"*20)

device.debug(device.read_attribute("short_scalar"))
# Read attribute using read_attribute
device.info(device.read_attribute("short_scalar").value)
# Read attribute as object attribute
device.info(device.short_scalar)
# Read multiple attributes with read_attributes
values = device.read_attributes(["short_scalar", "double_scalar", "boolean_scalar"])
device.debug(values)
device.info("Values read together:")
for value in values:
    device.info(f"     {value.name}: {value.value}")

```

## TaurusAttribute
A TaurusAttribute is an abstraction of a single attribute in your control system. This object provides convenient methods and properties to read, write, and subscribe to changes in the attribute value. You can also access the attribute’s metadata (e.g. label, data type, range) in a straightforward way.

```
import taurus
# taurus.setLogLevel(taurus.Debug)

attribute = taurus.Attribute("sys/tg_test/1/short_scalar")

attribute.info("Reading the attribute using read()")
reading = attribute.read()
attribute.info(f"Reading object: {reading}")
attribute.info(f"Current value (reading.value): {reading.value}")

attribute.info("-" * 20)

attribute.info("Reading the value directly via the property 'attribute.value'")
attribute.info(f"Attribute value: {attribute.value}")

attribute.info("-" * 20)

attribute.info("Attempting to write a new value (if the attribute is writable)")
try:
    attribute.write(42)  # Change this to a suitable value for your attribute's type
    attribute.info(f"New value after write: {attribute.value}")
except Exception as e:
    attribute.error(f"Failed to write value: {e}")

attribute.info("-" * 20)

attribute.info("Subscribing to changes (if the system supports events)")

def my_attr_listener(evt_src, evt_type, evt_value):
    attribute.info(f"Event received: {evt_type}, value: {evt_value.rvalue}")

attribute.addListener(my_attr_listener)

attribute.info("-" * 20)

attribute.info("Retrieving attribute configuration (metadata)")
config = attribute.getConfig()
attribute.info(f"Label: {config.label}, Data Type: {config.type}, Description: {config.description}")
```